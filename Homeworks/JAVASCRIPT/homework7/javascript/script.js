const arr = ['hello', 'world', 'Kiev', 'Kharkiv', 'Odessa', 'Lviv'];
function showList (arr) {
    const list = document.createElement('ul');
    const liArray = arr.map(item => `<li>${item}</li>`);
    list.innerHTML = liArray.join('');
    document.body.append(list);
}
showList(arr);

function timer (){
    const timer = document.createElement('p');
    let counter = 10;
    timer.innerText = counter;
    setInterval(() => {
        counter--;
        timer.innerText = counter;
        if (counter === 0) {
            document.body.innerHTML = '';
        }
    }, 1000);
    document.body.append(timer);
}
timer();