function getValues() {

    let num1 = '';
    let num2 = '';
    do {
        num1 = prompt('Введите первое число', num1);
        num2 = prompt('Введите второе число', num2);
    } while (isNaN(+num1) || isNaN(+num2) || num1 === "" || num2 === "");
    const operator = prompt('Введите оператор', '');

    console.log(calculate(+num1, +num2, operator));
}

function calculate(num1, num2, operator) {
    switch (operator) {
        case '+':
            return num1 + num2;

        case '*':
            return num1 * num2;

        case '-':
            return num1 - num2;

        case '/':
            return num1 / num2;
    }
}

getValues();

