# pe-18-fedorchuk
1. Описать своими словами, для чего вообще нужны функции в программировании?

Функция - это состоящий из одной или множества команд фрагмент кода, который можно вызывать. С помощью функции
можно повторить один и тот же набор действий в разных частях программы.

***

2. Описать своими словами, зачем в функцию передавать аргумент?

Аргумент - это значение, которое передается в функцию. Мы можем присвоить эти значения в переменные внутри функции, 
и с помощью этих параметров выполнять функцию.
