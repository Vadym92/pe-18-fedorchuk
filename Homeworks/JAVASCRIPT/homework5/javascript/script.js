function createNewUser() {
    let userName = prompt("Enter your name", '');
    let userLastName = prompt("Enter your lastname", '');
    let userBirthday = prompt("Enter your birthday", 'dd.mm.yyyy');

    const newUser = {
        firstName: userName,
        lastName: userLastName,
        birthday: userBirthday,

        getAge: function () {
            let dateArr = this.birthday.split('.');
            let formattedString = dateArr.reverse().join('-');
            let ageDiffMs = Date.now() - new Date(formattedString);
            let ageDiff = new Date(ageDiffMs).getFullYear();
            let userAge = ageDiff - 1970;
            return userAge;
            },

        getPassword: function () {
            return this.firstName[0].toUpperCase() + this.lastName.toLowerCase() + this.birthday.slice(6);
        },
    };
    return newUser;
}
const user = createNewUser();

console.log(user.getAge());
console.log(user.getPassword());


