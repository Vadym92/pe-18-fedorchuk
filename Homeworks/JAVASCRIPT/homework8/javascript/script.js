const wrapper = document.querySelector('.input_wrapper');
const input = document.getElementById('price');


function focusOnInput() {
    input.style.borderColor = 'green';
}


function blurInput() {
        const checkErrorSpan = document.querySelector('.error_text');
    if (checkErrorSpan) {
        checkErrorSpan.remove();
    }
    if (input.value < 0) {
        const error = document.createElement('p');
        error.classList.add('error_text');
        error.innerText = 'Please enter correct price!';
        input.style.borderColor = 'red';
        input.style.color = 'red';
        document.body.appendChild(error);
        return true;
    }

    input.style.borderColor = '';
    const createSpan = document.createElement('span');
    createSpan.classList.add('span_price');
    createSpan.innerText = `Текущая цена: ${input.value}`;
    input.style.color = 'green';

    const closeBtn = document.createElement('button');
    closeBtn.innerText = 'X';
    closeBtn.classList.add('del_btn');
    closeBtn.addEventListener('click', () => {
        input.value = '';
        createSpan.remove();
    });
    createSpan.append(closeBtn);
    wrapper.append(createSpan);

    if (input.value === '') {
        createSpan.remove();
    }
}

input.addEventListener('focus', focusOnInput);
input.addEventListener("blur", blurInput);

