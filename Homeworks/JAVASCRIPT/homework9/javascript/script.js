function focusOnTab(event) {
    let listContent = document.querySelector(".tabs-content li.active");

    if (listContent) {
        listContent.classList.remove("active");
    }

    let tabActive = document.querySelector(".tabs-title.active");
    if (tabActive) {
        tabActive.classList.remove("active");
    }

    tabActive = event.target.closest("li");
    tabActive.classList.add("active");
    const number = tabActive.dataset.number;
    const tabsContent = document.querySelectorAll(".tabs-content li");
    listContent = Array.from(tabsContent).find(item => (item.dataset.number === number));
    listContent.classList.add("active");
}
const tabs = document.querySelector(".tabs");
tabs.addEventListener("click", focusOnTab);