function filterBy(arr, type) {
    const filteredArr = arr.filter(function(item){
        if (type === 'object' && item === null){
            return true;
        }
        return typeof(item)  !== type;
    });
    return filteredArr;
}

console.log(filterBy(['hello', 45, 'world', 23, '23', true, false, null, [1,2,3]], ''));



