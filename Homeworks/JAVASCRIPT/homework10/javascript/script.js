const inputs = document.querySelectorAll('.password');
const icons = document.querySelectorAll('.icon-password');
const errorSpan = document.querySelector('.errorSpan');

icons.forEach((item, index) => {
    console.log("element icon: " + item);
    console.log("element index: " + index);
    item.addEventListener('click', () => changeIcon(inputs[index], item ))
});

function changeIcon (input, icon) {
    if (input.type === 'password') {
        icon.classList.replace('fa-eye-slash', 'fa-eye');
        input.type = 'text';
    } else {
        icon.classList.replace('fa-eye', 'fa-eye-slash');
        input.type = 'password';
    }
}

function verified () {
    errorSpan.innerHTML = "";
    const verInput1 = document.getElementById('input1');
    const verInput2 = document.getElementById('input2');

    if (verInput1.value === verInput2.value) {
        alert('You are Welcome');
    } else {
        errorSpan.innerHTML = 'Нужно ввести одинаковые значения';
    }
}

inputs.forEach(input => {
    input.addEventListener('input', clearError);
});

function clearError () {
    errorSpan.innerHTML = '';
}

const btn = document.querySelector('.btn');
btn.addEventListener('click',verified);
